//
//  LocationSearch.swift
//  WeatherApp-KocevaA
//
//  Created by Angela Koceva on 10/10/19.
//  Copyright © 2019 Angela Koceva. All rights reserved.
//

struct LocationSearchModel: Decodable {
    let title: String?
    let location_type: String?
    let latt_long: String?
    let woeid: Int?
}
