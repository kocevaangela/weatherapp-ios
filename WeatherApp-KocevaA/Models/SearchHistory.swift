//
//  SearchHistory.swift
//  WeatherApp-KocevaA
//
//  Created by Angela Koceva on 10/10/19.
//  Copyright © 2019 Angela Koceva. All rights reserved.
//

import Foundation

struct SearchHistory {
    enum SearchHistoryType {
        case query
    }
    
    let type: SearchHistoryType
    let string: String
    let timestamp: Date = Date()
}
