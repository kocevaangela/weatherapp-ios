//
//  DayForecastTableViewController.swift
//  WeatherApp-KocevaA
//
//  Created by Angela Koceva on 10/11/19.
//  Copyright © 2019 Angela Koceva. All rights reserved.
//

import UIKit
import HealthKit
import ReachabilitySwift
import NotificationBannerSwift

class DayDetailsTableViewController: UITableViewController {
    
    static let segueIdentifier = "detailsTableViewControllerIdentifier"

    var detailsData: LocationModel!
    private let numberFormatter = NumberFormatter()
    private let dateFormatter = DateFormatter()
    private let dateFormatterMedium = DateFormatter()
    
    var selectedDay: Int = 0
    var allData: Int = 0
    var citySearch: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatterMedium.dateStyle = .medium
        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 2
        title = detailsData.title
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NetworkReachability.sharedInstance.addListener(listener: self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NetworkReachability.sharedInstance.removeListener(listener: self)
    }

    // MARK: - Table view delegate

    override func numberOfSections(in tableView: UITableView) -> Int {
        if (citySearch == true) {
            return detailsData.consolidatedWeather.count
        } else {
            return 1
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.deselectRow(at: indexPath, animated: true)
        var dayData: LocationModel.ConsolidatedWeather
        if (citySearch == true) {
            dayData = detailsData.consolidatedWeather[indexPath.section]
        } else {
            dayData = detailsData.consolidatedWeather[selectedDay]
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.dayDetailsCellIdentifier, for: indexPath) as! DayDetailsTableViewCell
        cell.lblSummary.text = dayData.weather_state_name
        cell.lblMaxTemp.text = String(format: "%.0f" + " °C", dayData.max_temp)
        cell.lblMinTemp.text = String(format: "%.0f" + " °C", dayData.min_temp)
        
        let speed = dayData.wind_speed * 1.61
        cell.lblWindSpeed.text = String(format: "%.1f " + "km/h", speed)
        
        cell.lblWindDirection.text = dayData.wind_direction_compass
        cell.lblAirPressure.text = String(format: "%.2f" + " mbar", dayData.air_pressure)
        cell.lblHumidity.text = String(format: "%.0d%%", dayData.humidity)
        
        let visibility = dayData.visibility * 1.61
        cell.lblVisibility.text = String(format: "%.0f" + "km", visibility)
        cell.lblPredictibility.text = String(format: "%.0d%%", dayData.predictability)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (citySearch == true) {
            guard let applicableDate = detailsData?.consolidatedWeather[section].applicable_date,
                let date = dateFormatter.date(from: applicableDate)
                else { return nil }
            return dateFormatterMedium.string(from: date)
        } else {
            guard let applicableDate = detailsData?.consolidatedWeather[selectedDay].applicable_date,
                let date = dateFormatter.date(from: applicableDate)
                else { return nil }
            return dateFormatterMedium.string(from: date)
        }
    }
}

extension DayDetailsTableViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        switch status {
        case .notReachable:
            print("DayDetailsTableViewController: Network became unreachable")
        case .reachableViaWiFi:
            print("DayDetailsTableViewController: Network reachable through WiFi")
        case .reachableViaWWAN:
            print("DayDetailsTableViewController: Network reachable through Cellular Data")
        }
        if status == .notReachable {
            let banner = NotificationBanner(title: "No internet connection", subtitle: "Please check your internet connection", style: .danger)
            banner.show()
        }
    }
    
}
