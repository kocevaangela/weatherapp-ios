//
//  ViewController.swift
//  WeatherApp-KocevaA
//
//  Created by Angela Koceva on 10/10/19.
//  Copyright © 2019 Angela Koceva. All rights reserved.
//

import UIKit
import Toast_Swift
import ReachabilitySwift
import NotificationBannerSwift

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
 
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var currentDay: UILabel!
    @IBOutlet weak var currentState: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var chooseAnotherLocationButton: UIBarButtonItem!
    
    var dataSource: LocationModel?
    
    var woeid: Int = 839722
    
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    
    private let dateFormatter = DateFormatter()
    private let dateFormatterMedium = DateFormatter()
    private let numberFormatter = NumberFormatter()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
        }
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatterMedium.dateStyle = .medium
        addActivityIndicator()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupData()
        navigationController?.isToolbarHidden = false
        NetworkReachability.sharedInstance.addListener(listener: self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NetworkReachability.sharedInstance.removeListener(listener: self)
    }
    
    /**
     Initial data setup
     */
    func setupData() {
        activityIndicator.startAnimating()
        self.alphaIfNeeded(enabled: false)

        APIClient.sharedInstance.locationDetails(id: woeid) { (data, response, error) in
            if let error = error {
                print("Error: \(error.localizedDescription)")
                self.view.makeToast("Some error occured")
            } else {
                DispatchQueue.main.async {
                    self.dataSource = data
                    self.currentDay.text = self.dataSource?.title
                    if let temperature = self.dataSource?.consolidatedWeather[0].the_temp {
                        let temp = String(format: "%.0f", temperature)
                        self.temperature.text = (temp + " °C")
                    }
                    if let image = self.dataSource?.consolidatedWeather[0].weather_state_abbr {
                        self.weatherImage.image = UIImage(named: image + ".png")
                    }
                    if let currentState = self.dataSource?.consolidatedWeather[0].weather_state_name {
                        self.currentState.text = currentState
                    }
                    self.collectionView.reloadData()
                    self.activityIndicator.stopAnimating()
                    self.alphaIfNeeded(enabled: true)
                }
            }
        }
    }
    
    /**
     Formats date into the correct format
     */
    func formatedDate(indexPath: IndexPath) -> String {
        guard let applicableDate = dataSource?.consolidatedWeather[indexPath.row].applicable_date,
            let date = dateFormatter.date(from: applicableDate)
            else { return "" }
        
        return dateFormatterMedium.string(from: date)
    }
    
    /**
     Activity indicator as right bar button item
     */
    private func addActivityIndicator() {
        DispatchQueue.main.async {
            self.activityIndicator.hidesWhenStopped = true
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.activityIndicator)
        }
    }
    
    /**
     Adds alfa to the view when activity indicator is active
     - parameters:
        - enabled: Bool value
     */
    private func alphaIfNeeded(enabled: Bool) {
        view.isUserInteractionEnabled = enabled
        enabled ? activityIndicator.stopAnimating() : activityIndicator.startAnimating()
        UIView.animate(withDuration: 0.2) { [weak self] in
            DispatchQueue.main.async {
                self?.view.alpha = enabled ? 1.0 : 0.5
            }
        }
    }

    //MARK: UISegmentedControl
    /**
     List of three previously selected cities added in a segmentedControl.
     Depending on which one the user selects the view gets updated
     */
    @IBAction func segmentControlTapped(_ sender: Any) {
        let index = segmentControl.selectedSegmentIndex
        switch index {
        case 0:
            //City: Sofia
            woeid = 839722
            print("Sofia got selected")
        case 1:
            //City: New York
            woeid = 2459115
            print("New York got selected")
        case 2:
            //City: Tokyo
            woeid = 1118370
            print("Tokyo got selected")
        default:
            print("None of the cities are selected")
        }
        setupData()
    }
    
    
    //MARK: CollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource?.consolidatedWeather.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.dayCellIdentifier, for: indexPath) as? DaysDataCollectionViewCell else {
            return UICollectionViewCell()
        }
        if dataSource?.consolidatedWeather.count == 0 {
            return cell
        }
        
        let dayForecast = dataSource?.consolidatedWeather[indexPath.row]
        
        if let minTemp = dayForecast?.min_temp,
            let maxTemp = dayForecast?.max_temp,
            let image = dayForecast?.weather_state_abbr {
            
                let minTemp = String(format: "%.0f", minTemp)
                cell.lowTemp.text = ("Lo " + minTemp + " °C")
            
                let maxTemp = String(format: "%.0f", maxTemp)
                cell.highTemp.text = ("Hi " + maxTemp + " °C")
                cell.image.image = UIImage(named: image + ".png")
                cell.day.text = formatedDate(indexPath: indexPath)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let dayDetailsTableViewController = storyboard?.instantiateViewController(withIdentifier: Constants.detailsTableViewControllerIdentifier) as? DayDetailsTableViewController {
            dayDetailsTableViewController.detailsData = dataSource
            dayDetailsTableViewController.selectedDay = indexPath.row
            dayDetailsTableViewController.tableView.reloadData()

            self.navigationController?.pushViewController(dayDetailsTableViewController, animated: true)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
}

extension MainViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        switch status {
        case .notReachable:
            print("MainViewController: Network became unreachable")
        case .reachableViaWiFi:
            print("MainViewController: Network reachable through WiFi")
        case .reachableViaWWAN:
            print("MainViewController: Network reachable through Cellular Data")
        }
        chooseAnotherLocationButton.isEnabled = !(status == .notReachable)
        DispatchQueue.main.async {
            self.segmentControl.isEnabled = !(status == .notReachable)
        }
        if status == .notReachable {
            let banner = NotificationBanner(title: "No internet connection", subtitle: "Please check your internet connection", style: .danger)
            banner.show()
        }
    }
    
}

