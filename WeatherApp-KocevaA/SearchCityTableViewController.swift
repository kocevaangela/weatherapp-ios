//
//  TableViewController.swift
//  WeatherApp-KocevaA
//
//  Created by Angela Koceva on 10/11/19.
//  Copyright © 2019 Angela Koceva. All rights reserved.
//

import UIKit
import Toast_Swift
import ReachabilitySwift
import NotificationBannerSwift

class SearchCityTableViewController: UITableViewController, UISearchBarDelegate {

    var dataSource: [LocationSearchModel] = [LocationSearchModel]()
    var searchController: UISearchController = UISearchController(searchResultsController: nil)
    var searchResults: [SearchHistory] = [SearchHistory]()
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    let dateFormatter = DateFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        tableView.delegate = self
        searchController.delegate = self as? UISearchControllerDelegate
        navigationController?.isToolbarHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        addSearchBar()
        addActivityIndicator()
        NetworkReachability.sharedInstance.addListener(listener: self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NetworkReachability.sharedInstance.removeListener(listener: self)
    }
    
    private func addActivityIndicator() {
        activityIndicator.hidesWhenStopped = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator)
    }
    
    private func alphaIfNeeded(enabled: Bool) {
        searchController.searchBar.isUserInteractionEnabled = enabled
        tableView.isUserInteractionEnabled = enabled
        enabled ? activityIndicator.stopAnimating() : activityIndicator.startAnimating()
        UIView.animate(withDuration: 0.2) { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.alpha = enabled ? 1.0 : 0.5
                self?.searchController.searchBar.alpha = enabled ? 1.0 : 0.5
            }
        }
    }
    
    private func addSearchBar() {
        searchController.searchResultsUpdater = self as? UISearchResultsUpdating
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "Enter location here..."
        searchController.searchBar.sizeToFit()
        tableView.tableHeaderView = searchController.searchBar
        searchController.definesPresentationContext = true
        searchController.searchBar.barTintColor = #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1)
        searchController.searchBar.tintColor = #colorLiteral(red: 0.3607843137, green: 0.5254901961, blue: 0.968627451, alpha: 1)
    }
    
    /**
     Gets results data for a query
     */
    func fetchQueryData(query: String, type: SearchHistory.SearchHistoryType = .query) {
        activityIndicator.startAnimating()
        alphaIfNeeded(enabled: false)
        APIClient.sharedInstance.locationSearch(query: query) { [weak self] (data, response, error) in
            if let error = error {
                print("Error: \(error.localizedDescription)")
                self?.view.makeToast("Can't load data, please try again")
            } else {
                if let data = data {
                    if data.count == 0 {
                        DispatchQueue.main.async {
                            self?.tableView.makeToast("Can't find forecast for \(query)", duration: 3.0, position: .top)
                        }
                    } else {
                        self?.dataSource.removeAll()
                        self?.dataSource = data
                    }
                }
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                    self?.activityIndicator.stopAnimating()
                    self?.alphaIfNeeded(enabled: true)
                }
            }
        }
    }
    
    /**
     Gets results data for specific id
     */
    func fetchLocationDetails(id: Int) {
        activityIndicator.startAnimating()
        alphaIfNeeded(enabled: false)
        
        APIClient.sharedInstance.locationDetails(id: id) { [weak self] (location, response, error) in
            if let error = error {
                print("Error: \(error.localizedDescription)")
                self?.view.makeToast("Can't load data, please try again")
            } else {
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    self?.alphaIfNeeded(enabled: true)
                    if let dayDetailsTableViewController = self?.storyboard?.instantiateViewController(withIdentifier:Constants.detailsTableViewSegueIdentifier) as? DayDetailsTableViewController {
                        dayDetailsTableViewController.detailsData = location
                        dayDetailsTableViewController.citySearch = true
                        dayDetailsTableViewController.tableView.reloadData()
                        self?.navigationController?.pushViewController(dayDetailsTableViewController, animated: true)
                    }
                }
            }
        }
    }

    // MARK: - Table View Controller Delegate

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchController.isActive ? searchResults.count : dataSource.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "defaultCell", for: indexPath)
            
        cell.detailTextLabel?.text = ""
        cell.textLabel?.text = ""
            
        let locationSearch = dataSource[indexPath.row]
            
        var title = ""
            
        if let locationTitle = locationSearch.title {
            title = locationTitle
        }
            
        cell.textLabel?.text = title
            
        if let woeid = locationSearch.woeid {
            cell.detailTextLabel?.text = String(woeid)
        }
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

            if let woeid = dataSource[indexPath.row].woeid {
                fetchLocationDetails(id: woeid)
            }
    }
    
        
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let searchBarText = searchBar.text, !searchBarText.isEmpty {
            searchController.isActive = false
            fetchQueryData(query: searchBarText)
        }
    }
        
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        tableView.reloadData()
    }

}

extension SearchCityTableViewController: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        switch status {
        case .notReachable:
            print("SearchCityTableViewController: Network became unreachable")
        case .reachableViaWiFi:
            print("SearchCityTableViewController: Network reachable through WiFi")
        case .reachableViaWWAN:
            print("SearchCityTableViewController: Network reachable through Cellular Data")
        }
        if status == .notReachable {
            let banner = NotificationBanner(title: "No internet connection", subtitle: "Please check your internet connection", style: .danger)
            banner.show()
        }
    }
    
}
