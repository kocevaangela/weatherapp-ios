//
//  Constants.swift
//  WeatherApp-KocevaA
//
//  Created by Angela Koceva on 10/10/19.
//  Copyright © 2019 Angela Koceva. All rights reserved.
//

import UIKit

class Constants: NSObject {
    
    static let dayCellIdentifier = "dayCell"

    static let dayDetailsCellIdentifier = "detailsTableViewCell"
    static let detailsTableViewControllerIdentifier = "detailsTableViewControllerIdentifier"
    
    static let searchCityTableViewControllerIdentifier = "searchCityTableViewControllerSegueIdentifier"
    static let searchCityViewControllerIdentifier = "searchCitySegueIdentifier"
    static let detailsTableViewSegueIdentifier = "detailsTableViewControllerIdentifier"

}
