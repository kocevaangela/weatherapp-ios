//
//  DayDetailsTableViewCell.swift
//  WeatherApp-KocevaA
//
//  Created by Angela Koceva on 10/11/19.
//  Copyright © 2019 Angela Koceva. All rights reserved.
//

import UIKit

class DayDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblSummary: UILabel!
    @IBOutlet weak var lblMinTemp: UILabel!
    @IBOutlet weak var lblMaxTemp: UILabel!
    @IBOutlet weak var lblWindSpeed: UILabel!
    @IBOutlet weak var lblWindDirection: UILabel!
    @IBOutlet weak var lblAirPressure: UILabel!
    @IBOutlet weak var lblHumidity: UILabel!
    @IBOutlet weak var lblVisibility: UILabel!
    @IBOutlet weak var lblPredictibility: UILabel!
    
    static let identifier = "daysCell"
    static let cellClass = "NewDayDetailsCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
