//
//  DayDataCollectionViewCell.swift
//  WeatherApp-KocevaA
//
//  Created by Angela Koceva on 10/10/19.
//  Copyright © 2019 Angela Koceva. All rights reserved.
//

import UIKit

class DaysDataCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var day: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var highTemp: UILabel!
    @IBOutlet weak var lowTemp: UILabel!

}
