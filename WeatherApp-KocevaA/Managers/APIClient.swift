//
//  APIClient.swift
//  WeatherApp-KocevaA
//
//  Created by Angela Koceva on 10/10/19.
//  Copyright © 2019 Angela Koceva. All rights reserved.
//

import UIKit

class APIClient: NSObject {
    
    static let sharedInstance = APIClient()
    var urlComponents: URLComponents = URLComponents()
    let allDetails = [LocationModel]()
    
    override init() {
        super.init()
        urlComponents.scheme = "https"
        urlComponents.host = "www.metaweather.com"
        urlComponents.path = "/api/location/search/"
    }
    
    typealias dataCompletionHandler = (_ data: [LocationSearchModel]?, _ response: URLResponse?, _ error: Error?) -> ()
    typealias dataLocationDetailsCompletionHandler = (_ data: LocationModel?, _ response: URLResponse?, _ error: Error?) -> ()

    /**
     Text to search for.
     - parameters:
        - query: e.g City name
     */
    func locationSearch(query: String, completionHandler: @escaping dataCompletionHandler) {
        urlComponents.path = "/api/location/search/"

        DataManager.shared.searchHistory.append(SearchHistory(type: .query, string: query))
        urlComponents.queryItems = [URLQueryItem(name: "query", value: query)]
        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        session.dataTask(with: request) { (data, response, error) in
            if let data = data {
                completionHandler(APIClient.parseSearchJSON(data: data), response, error)
            } else {
                completionHandler(nil, response, error)
            }
        }.resume()
    }
    
    /**
     Get data for a location by entering a woeid
     - parameters:
        - id: City identification number
     */
    func locationDetails(id: Int, completionHandler: @escaping dataLocationDetailsCompletionHandler) {
        urlComponents.path = "/api/location/\(String(id))/"
        urlComponents.queryItems = nil

        guard let url = urlComponents.url else { fatalError("Could't create URL") }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        session.dataTask(with: request) { (data, response, error) in
            if let data = data {
                completionHandler(APIClient.parseLocationJSON(data: data), response, error)
            } else {
                completionHandler(nil, response, error)
            }
        }.resume()
    }
    
    /**
     Parses data gotten from location search and returns it as LocationSearchModel
     - Parameters:
        - data: Data gotten from location search
     */
     private static func parseSearchJSON(data: Data) -> [LocationSearchModel]? {
        do {
            let decoder = JSONDecoder()
            let parsedData = try decoder.decode([LocationSearchModel].self, from: data)
            return parsedData
            } catch let error {
                print("Error: \(error)")
            return nil
        }
    }
    
    /**
     Parses data gotten from location search and returns it as LocationModel
     - Parameters:
        - data: Data gotten from location search
     */
    private static func parseLocationJSON(data: Data) -> LocationModel? {
        do {
            let decoder = JSONDecoder()
            let parsedData = try decoder.decode(LocationModel.self, from: data)
            return parsedData
        } catch let error {
            print("Error: \(error)")
            return nil
        }
    }
}

extension APIClient: URLSessionDelegate {
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
}
