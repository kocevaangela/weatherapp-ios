//
//  DataManager.swift
//  WeatherApp-KocevaA
//
//  Created by Angela Koceva on 10/10/19.
//  Copyright © 2019 Angela Koceva. All rights reserved.
//

import UIKit

class DataManager {
    static let shared: DataManager = DataManager()
    var array: [LocationSearchModel] = [LocationSearchModel]()
    var searchHistory: [SearchHistory] = [SearchHistory]()
}
