//
//  NetworkReachability.swift
//  WeatherApp-KocevaA development
//
//  Created by Angela Koceva on 10/12/19.
//  Copyright © 2019 Angela Koceva. All rights reserved.
//

import UIKit
import ReachabilitySwift

class ReachabilityManager: NSObject {
   
    static  let shared = ReachabilityManager()
    
    var isNetworkAvailable : Bool {
        return reachabilityStatus != .notReachable
    }
    
    var reachabilityStatus: Reachability.NetworkStatus = .notReachable
    let reachability = Reachability()!
    
    @objc func reachabilityChanged(notification: Notification) {
        let reachability = notification.object as! Reachability
        switch reachability.currentReachabilityStatus {
        case .notReachable:
            print("Network became unreachable")
        case .reachableViaWiFi:
           print("Network reachable through WiFi")
        case .reachableViaWWAN:
            print("Network reachable through Cellular Data")
        }
    }
    
    func startMonitoring() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reachabilityChanged),
                                               name: ReachabilityChangedNotification,
                                               object: reachability)
        do {
            try reachability.startNotifier()
        } catch {
             print("Could not start reachability notifier")
        }
    }
    
    func stopMonitoring(){
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self,
                                                  name: ReachabilityChangedNotification,
                                                  object: reachability)
    }

}
