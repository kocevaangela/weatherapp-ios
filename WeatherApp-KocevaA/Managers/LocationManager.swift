//
//  LocationManager.swift
//  WeatherApp-KocevaA
//
//  Created by Angela Koceva on 10/10/19.
//  Copyright © 2019 Angela Koceva. All rights reserved.
//

import UIKit
import CoreLocation

protocol LocationManagerDelegate: class {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
}

class LocationManager: NSObject {
    
    static let shared: LocationManager = LocationManager()
    private let locationManager: CLLocationManager = CLLocationManager()
    var delegate: LocationManagerDelegate?
    var location: CLLocation?
    
    typealias completionHandler = () -> ()
    private var managerCompletionHander: completionHandler?
    
    private override init() {
        super.init()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func requestLocationAuthorization() {
        locationManager.requestWhenInUseAuthorization()
    }
    
    func requestLocation(completionHandler: @escaping () -> ()) {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestLocation()
            self.managerCompletionHander = completionHandler
        }
    }
    
}
